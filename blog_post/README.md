# Notes
If you start a debug session in Webstorm targetting index.html it will open it in your browser and hot reload the content, convenient for development sessions.
Prefer classes to IDs for styling, even if that means only one element has that class.
How to organize CSS: https://9elements.com/css-rule-order/
