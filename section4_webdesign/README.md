# Notes
- Font-size between 15 and 25 pixels for body text --> isn't that an issue with screens of various definition or for accessibility? Does he mean to give boundaries to how to use "rem", such as the default rem is 16px so ~0.94rem is the minimum and ~1.6rem is the maximum?
- Headlines can be much bigger (30, 60, 90px). Play with font-weight to make it less overwhelming
- Line spacing between 120 and 150% --> unless specific styling play, like quotes on medium?
- 45 to 90 characters per line
- Fonts:
  Use good fonts, use serif fonts for storytelling or long reading (Google web fonts Sans-Serif: Open Sans, Lato, Raleway, Monsterrat, PT Sans... ; Serif: Cardo, Merriweather, PT Serif...)
  Choose based on website desired look and feel, prefer one typeface until confident in using a couple consistently
- Color:
  Use one base color, use color wheel tools to get color pairings following color theory
  One way to do things: mostly greyscale and use one color to draw attention
  Never use black --> unless it's your brand I suppose?
  Think about the sentiments behind colors --> dependent on culture
  --> How about using colors from an IDE theme, like Solaris?
- Images:
  Overlay images with a color or gradient before putting a text on top, to avoid contrast issues. Alternative: put the text in a box, blur the image (completely or use an out-of-focus area but careful about how the text will move over the image as the clients' screen changes), floor-fade (overlay color, mostly black, fading in at the bottom).
- Icons:
  Use icons to list features/steps, actions/links (label your icons)
  Use icon fonts whenever possible (as opposed to images)
- Spacing and Layout:
  Use whitespace between elements, groups of elements, sections, just not too much that you loose the relations
  Use whitespace to help define hierarchy
- Inspiration:
  Collect designs that you like, then: why do they look good, what do they have in common, how were they built in HTML/CSS?