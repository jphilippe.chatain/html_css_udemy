const stickyNavOffset = 60;

function seesHalfOfElementOrMore(offsetTop, height) {
    return window.scrollY > offsetTop - (height / 2)
}

function seesQuarterOfElementOrLess(offsetTop, height) {
    return window.scrollY + window.innerHeight < offsetTop + 0.25 * height;
}

function handleScroll() {
    if (window.scrollY > firstSection.offsetTop - stickyNavOffset) {
        nav.classList.add("sticky");
    } else {
        nav.classList.remove("sticky");
    }
    if (seesHalfOfElementOrMore(firstSection.offsetTop, firstSection.offsetHeight)) {
        features.forEach(feature => feature.classList.add('animate__animated', 'animate__fadeIn'));
    } else if (seesQuarterOfElementOrLess(firstSection.offsetTop, firstSection.offsetHeight)) {
        features.forEach(feature => {
            feature.style.opacity = "0";
            feature.classList.remove('animate__animated', 'animate__fadeIn')
        });
    }
    if (seesHalfOfElementOrMore(stepsSection.offsetTop, stepsSection.offsetHeight)) {
        appScreen.classList.add('animate__animated', 'animate__fadeInUpBig');
    } else if (seesQuarterOfElementOrLess(stepsSection.offsetTop, stepsSection.offsetHeight)) {
        appScreen.style.opacity = "0";
        appScreen.classList.remove('animate__animated', 'animate__fadeInUpBig');
    }
}

const nav = document.querySelector("nav");
const firstSection = document.querySelector("section:first-of-type");
const features = document.querySelectorAll(".section-features .col");
const stepsSection = document.querySelector("#steps");
const appScreen = document.querySelector(".app-screen");

document.addEventListener("scroll", handleScroll);

const mobileMenu = document.querySelector(".mobile-nav-icon");
const mainNav = document.querySelector(".main-nav");
// TODO: This hardcoded value is not great, should be obtainable from mainNav but couldn't yet.
const navHeight = 152;
let isMobileNavOpen = false;
mobileMenu.addEventListener("click", () => {
    if (isMobileNavOpen) {
        mainNav.style.marginTop = "-" + navHeight + "px";
        mobileMenu.querySelector("ion-icon").name = "menu";

        setTimeout(() => {
            mainNav.style.display = "none";
        }, 500)
    } else {
        mainNav.style.display = "block";

        setTimeout(() => {
            mainNav.style.marginTop = "10px";
            mobileMenu.querySelector("ion-icon").name = "close-outline";
        }, 10)
    }
    isMobileNavOpen = !isMobileNavOpen;
});