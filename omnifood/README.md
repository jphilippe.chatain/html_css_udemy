# Website creation and development process
- Define the project
    - Goal
    - Audience
- Plan the content
    - text, images, videos, icons, etc.
    - think about visual hierarchy
    - define navigation
    - define site structure (big projects)
- Sketch your ideas
    - get inspired
    - get the ideas out of your head
    - make as many sketches as you want, don't make them perfect
    - never start designing before knowing what you want to build
- Design the website
    - use guidelines and tips from previous sections
    - use HTML/CSS - design in the browser --> sounds a bit strange, makes me think that you would loose a lot of time trying to find the right CSS for each iteration
    - use sketches, content and planning decisions made in previous steps
- Optimize
    - site speed
    - SEO
- Launch (host)
- Maintain
    - monitor, fix
    - update content
    
# Standard breakpoints
480, 768, 1024, 1200 px
Can start by doing media queries based on device width
Later (more XP) you should set breakpoints wherever your design starts looking bad as the width reduces.

# CSS Browser prefixes
Android:    -webkit-
Chrome:     -webkit-
Firefox:    -moz-
IE:         -ms-
iOS:        -webkit-
Opera:      -o-
Safari:     -webkit-

--> This tool (https://github.com/postcss/autoprefixer) can be used to add the prefixes automatically as part of a pipeline. That way the repo's code stays readable. Only do locally to test and troubleshoot.